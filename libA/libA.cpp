//
// Created by Konstantin Skornyakov on 02.09.2021.
//

#include "libA.h"

#include <thread>

void libA() {
    using namespace std::chrono_literals;
    std::this_thread::sleep_for(1s);
}